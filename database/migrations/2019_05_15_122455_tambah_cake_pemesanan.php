<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahCakePemesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(!Schema::hasColumn('pemesanans', 'owner')){
                $table->string('owner');
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(Schema::hasColumn('pemesanans', 'owner')){
                $table->dropColumn('owner');
            }
        });
    }
}
