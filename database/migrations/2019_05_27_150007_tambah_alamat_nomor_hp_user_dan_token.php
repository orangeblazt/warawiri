<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahAlamatNomorHpUserDanToken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            if(!Schema::hasColumn('users', 'alamat')){
                $table->text('alamat')->nullable();
            }
            if(!Schema::hasColumn('users', 'no_hp')){
                $table->string('no_hp')->nullable();
            }
            if(!Schema::hasColumn('users', 'no_hp')){
                $table->text('token')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('users', function (Blueprint $table) {
            if(Schema::hasColumn('users', 'alamat')){
                $table->dropColumn('alamat');
            }
            if(Schema::hasColumn('users', 'no_hp')){
                $table->dropColumn('no_hp');
            }
            if(Schema::hasColumn('users', 'token')){
                $table->dropColumn('token');
            }
        });
    }
}
