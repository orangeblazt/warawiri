<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahMetodePembayaranPemesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(!Schema::hasColumn('pemesanans', 'metode_pembayaran')){
                $table->unsignedInteger('metode_pembayaran')->nullable();
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(Schema::hasColumn('pemesanans', 'metode_pembayaran')){
                $table->dropColumn('metode_pembayaran');
            }
        });
    }
}
