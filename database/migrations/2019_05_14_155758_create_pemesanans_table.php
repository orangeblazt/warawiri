<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePemesanansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pemesanans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nama_pembeli');
            $table->string('email');
            $table->string('nomor_hp');
            $table->unsignedInteger('jasa_pengiriman');
            $table->text('alamat_pengiriman');
            $table->text('catatan_pengiriman')->nullable();
            $table->unsignedInteger('target_donasi')->nullable();
            $table->bigInteger('nominal_donasi')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pemesanans');
    }
}
