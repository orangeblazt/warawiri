<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahStatusPemesanan extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(!Schema::hasColumn('pemesanans', 'status_pemesanan')){
                $table->unsignedInteger('status_pemesanan')->default(1);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pemesanans', function (Blueprint $table) {
            if(Schema::hasColumn('pemesanans', 'status_pemesanan')){
                $table->dropColumn('status_pemesanan');
            }
        });
    }
}
