<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahProdukTerjual extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('produks', function (Blueprint $table) {
            if(!Schema::hasColumn('produks', 'terjual')){
                $table->unsignedInteger('terjual')->default(0);
            }
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('produks', function (Blueprint $table) {
            if(Schema::hasColumn('produks', 'terjual')){
                $table->dropColumn('terjual');
            }
        });
    }
}
