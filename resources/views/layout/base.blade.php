<!DOCTYPE html>
<html>
<head>
	<!-- Favicon -->
	<link rel="icon" href="/img/favicon.ico">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Reponsive -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Meta Description -->
    <meta name="description" content="@yield('meta-description', 'Halal Market.')">

    <!-- Font -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Montserrat:400,700">

    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous"> -->
    <link rel="stylesheet" href="/fontaw/css/all.css">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Title -->
	<title>@yield('title', 'Halal Market')</title>

	<!-- </head> -->
</head>
<body>
    <main id="app">
        <div>
            @yield('header')
        </div>
	    <div id="promo">
	        @yield('promo')
	    </div>
	    <div id="content">
	    	@yield('content')
	    </div>
    </main>
    <footer>
        <div>
            Copyright © 2019 Halal Market
        </div>
        <hr/>
        <div>
            PT Sinergi Slaras Insani
        </div>
        <div>
            NIB No. 9120004691777
        </div>
        <div>
            NPWP 90.926.042.4-065.000
        </div>
    </footer>
    <!-- </body></html> -->
</body>
</html>