const tambahBarangMinimal1 = barang => {
    let cart = window.Vue.$storage.get('cart');
    
    // initialize on the first time
    if (!cart) {
        cart = [];
    }

    let foundIndex = -1;
    cart.find((b, i) => {
        if (b.data.id === barang.id) {
            foundIndex = i;
            return true;
        }

        return false;
    });

    if (foundIndex === -1) {
        cart.push({
            data: barang,
            qty: 1
        });
    }
    
    window.Vue.$storage.set('cart', cart);
    return barang;
}

const tambahBarang = barang => {
    let cart = window.Vue.$storage.get('cart');
    
    // initialize on the first time
    if (!cart) {
        cart = [];
    }

    let foundIndex = -1;
    cart.find((b, i) => {
        if (b.data.id === barang.id) {
            foundIndex = i;
            return true;
        }

        return false;
    });

    if (foundIndex === -1) {
        cart.push({
            data: barang,
            qty: 1
        });
    } else {
        cart[foundIndex].qty++;
    }
    window.Vue.$storage.set('cart', cart);
    return barang;
}

const tambahPemesanan = pemesanan => {
    let daftar_pemesanan = window.Vue.$storage.get('pemesanan');
    
    // initialize on the first time
    if (!daftar_pemesanan) {
        daftar_pemesanan = [pemesanan];
    } else {
        daftar_pemesanan.push(pemesanan);
    }

    window.Vue.$storage.set('pemesanan', daftar_pemesanan);
    window.Vue.$storage.set('cart', []);
    return pemesanan;
}

const kurangBarang = barang => {
    let cart = window.Vue.$storage.get('cart');
    let foundIndex = -1;
    cart.find((b, i) => {
        if (b.data.id === barang.id) {
            foundIndex = i;
            return true;
        }

        return false;
    });

    if (foundIndex !== -1) {
        cart[foundIndex].qty--;
        if(cart[foundIndex].qty === 0) {
            cart.splice(foundIndex, 1);
        }
    }
    window.Vue.$storage.set('cart', cart);
    return barang;
}

const keranjang = _ => {
    let cart = window.Vue.$storage.get('cart');
    return cart ? cart : [];
}

const pemesanan = _ => {
    let pemesanan = window.Vue.$storage.get('pemesanan');
    return pemesanan ? pemesanan : [];
}

const ambilBarang = barang => {
    let cart = window.Vue.$storage.get('cart');
    return cart ? cart.find(b => b.data.id === barang.id) : null;
}

const ambilPemesanan = pemesanan => {
    let cart = window.Vue.$storage.get('pemesanan');
    return cart ? cart.find(b => b.data.id === pemesanan.id) : null;
}

const getCake = _ => {
    return window.Vue.$storage.get('cake');
}

const saveCake = cake => {
    window.Vue.$storage.set('cake', cake);
    return cake;
}

const getCredential = _ => {
    return window.Vue.$storage.get('credential');
}

const saveCredential = cake => {
    window.Vue.$storage.set('credential', cake);
    return cake;
}

export {
    tambahBarangMinimal1,
    tambahBarang,
    tambahPemesanan,
    
    kurangBarang,
    
    ambilBarang,
    ambilPemesanan,

    keranjang,
    pemesanan,

    getCake,
    saveCake,

    getCredential,
    saveCredential
};
