
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue2Storage from 'vue2-storage';
Vue.use(Vue2Storage, {
    prefix: 'app_',
    driver: 'local',
    ttl: 60 * 60 * 24 * 1000
});

/**
 * Initialize user token
 */
import * as rest from './rest';
import * as store from './store';
(_ => {
    if(!store.getCake()) {
        rest.default.getANewCake()
            .then(cake => store.saveCake(cake))
            .catch(err => console.error(`error getting cake ${err}`));
    }
})();

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('example', require('./components/Example.vue'));
Vue.component('hm-button', require('./components/layout/HMButton.vue'));
Vue.component('promo', require('./components/layout/Promo.vue'));

Vue.component('top-bar-dashboard', require('./components/page/dashboard/TopBarDashboard.vue'));
Vue.component('dashboard', require('./components/page/dashboard/Dashboard.vue'));

Vue.component('top-bar-keranjang', require('./components/page/keranjang/TopBarKeranjang.vue'));
Vue.component('keranjang', require('./components/page/keranjang/Keranjang.vue'));

Vue.component('top-bar-checkout', require('./components/page/checkout/TopBarCheckout.vue'));
Vue.component('checkout', require('./components/page/checkout/Checkout.vue'));

Vue.component('top-bar-pencarian', require('./components/page/pencarian/TopBarPencarian.vue'));
Vue.component('pencarian', require('./components/page/pencarian/Pencarian.vue'));

Vue.component('top-bar-detail-produk', require('./components/page/detail-produk/TopBarDetailProduk.vue'));
Vue.component('detail-produk', require('./components/page/detail-produk/DetailProduk.vue'));

Vue.component('top-bar-pemesanan', require('./components/page/pemesanan/TopBarPemesanan.vue'));
Vue.component('pemesanan', require('./components/page/pemesanan/Pemesanan.vue'));

Vue.component('top-bar-detail-pemesanan', require('./components/page/detail-pemesanan/TopBarDetailPemesanan.vue'));
Vue.component('detail-pemesanan', require('./components/page/detail-pemesanan/DetailPemesanan.vue'));

Vue.component('top-bar-metode-pembayaran', require('./components/page/metode-pembayaran/TopBarMetodePembayaran.vue'));
Vue.component('metode-pembayaran', require('./components/page/metode-pembayaran/MetodePembayaran.vue'));

Vue.component('top-bar-autentikasi', require('./components/page/autentikasi/TopBarAutentikasi.vue'));
Vue.component('autentikasi', require('./components/page/autentikasi/Autentikasi.vue'));

Vue.component('top-bar-profil', require('./components/page/profil/TopBarProfil.vue'));
Vue.component('profil', require('./components/page/profil/Profil.vue'));

Vue.component('top-bar-chat', require('./components/page/chat/TopBarChat.vue'));
Vue.component('chat', require('./components/page/chat/Chat.vue'));

Vue.component('top-bar-daftar-chat', require('./components/page/daftar-chat/TopBarDaftarChat.vue'));
Vue.component('daftar-chat', require('./components/page/daftar-chat/DaftarChat.vue'));

const app = new Vue({
    el: '#app',
    created () {
        // The configuration of the plugin can be changed at any time.
        // Just call the setOptions method and pass the object with the settings to it.
        this.$storage.setOptions({
            prefix: 'app_',
            driver: 'local',
            ttl: 60 * 60 * 24 * 1000
        })
    }
});
