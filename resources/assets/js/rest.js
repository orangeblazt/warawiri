import * as store from './store';
export default {
	apiUrl: '/api/v1',
	get(url, resolve, reject) {
		axios.get(url, {
			headers: {
				"Accept": "application/json",
				"X-Auth": store.getCake(),
				"X-User-Id": store.getCredential() ? store.getCredential().id : null
			}
		}).then(response => {
			if (response && response.status === 200) {
				if (response.data && !response.data.error) {
					resolve(response.data.result);
				} else {
					reject(response.data.error);
				}
			} else {
				reject("Error");
			}
		}).catch(err => {
			reject(err);
		});
	},
	post(url, data, resolve, reject) {
		axios.post(url, data, {
			headers: {
				"Accept": "application/json",
				'Content-Type': 'application/json',
				"X-Auth": store.getCake(),
				"X-User-Id": store.getCredential() ? store.getCredential().id : null
			}
		}).then(response => {
			if (response && response.status === 200) {
				if (response.data && !response.data.error) {
					resolve(response.data.result);
				} else {
					reject(response.data.error);
				}
			} else {
				reject("Error");
			}
		}).catch(err => {
			reject(err);
		});
	},



	// API: -- GET --
	quoteApi() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/quote`;
			this.get(url, resolve, reject);
		});
	},
	getANewCake() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/give_me_a_cake`;
			this.get(url, resolve, reject);
		});
	},
	produkTerlaris() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/produk/kategori/terlaris`;
			this.get(url, resolve, reject);
		});
	},
	produkTerbaru() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/produk/kategori/terbaru`;
			this.get(url, resolve, reject);
		});
	},
	produkAll(q) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/produk${q ? `?q=${q}` : ''}`;
			this.get(url, resolve, reject);
		});
	},
	produkDetail(id) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/produk/${id}`;
			this.get(url, resolve, reject);
		});
	},
	daftarJasaPengiriman() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/const/jasa_pengiriman`;
			this.get(url, resolve, reject);
		});
	},
	daftarPartnerDonasi() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/const/partner_donasi`;
			this.get(url, resolve, reject);
		});
	},
	daftarMetodePembayaran() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/const/metode_pembayaran`;
			this.get(url, resolve, reject);
		});
	},
	semuaPemesanan() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/pemesanan`;
			this.get(url, resolve, reject);
		});
	},
	semuaPemesananCount() {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/pemesanan/count/all`;
			this.get(url, resolve, reject);
		});
	},


	// API: -- POST --
	ajukanPemesanan(data) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/pemesanan`;
			this.post(url, data, resolve, reject);
		});
	},
	pemesananDetail(id) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/pemesanan/${id}`;
			this.get(url, resolve, reject);
		});
	},
	login(data) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/user/login`;
			this.post(url, data, resolve, reject);
		});
	},
	register(data) {
		return new Promise((resolve, reject) => {
			const url = `${this.apiUrl}/user/register`;
			this.post(url, data, resolve, reject);
		});
	}
}
