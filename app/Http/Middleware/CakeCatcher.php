<?php

namespace App\Http\Middleware;

use Closure;

class CakeCatcher
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->cake = $request->header('X-Auth');
        $request->user_id = $request->header('X-User-Id');
        return $next($request);
    }
}
