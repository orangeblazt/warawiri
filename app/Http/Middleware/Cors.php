<?php

namespace App\Http\Middleware;

use Closure;

class Cors
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {        
        header("Access-Control-Allow-Origin: *");

        // ALLOW OPTIONS METHOD
        $headers = [
        
            'allow_origins' => [
                '*',
            ],
    
            'allow_methods' => [
                'POST',
                'GET',
                'OPTIONS',
                'PUT',
                'PATCH',
                'DELETE',
            ],
    
            'allow_headers' => [
                'Content-Type',
                'X-Auth-Token',
                'Origin',
                'Authorization',
            ],
    
            /*
             * Preflight request will respond with value for the max age header.
             */
            'max_age' => 60 * 60 * 24,
        ];
        
        if($request->getMethod() == "OPTIONS") {
            // The client-side application can set only headers allowed in Access-Control-Allow-Headers
            return Response::make('OK', 200, $headers);
        }

        $response = $next($request);
        foreach($headers as $key => $value)
            $response->header($key, $value);
        return $response;
    }
}
