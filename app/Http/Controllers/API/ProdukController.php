<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Produk;

class ProdukController extends Controller
{
    public function terlaris() {
        $allproduk = Produk::where('stok', '>', 0)->limit(3)->get();
        header('Content-Range: ' . sizeof($allproduk));
        return $this->res200($allproduk);
    }

    public function terbaru() {
        $allproduk = Produk::where('stok', '>', 0)->limit(3)->offset(3)->get();
        header('Content-Range: ' . sizeof($allproduk));
        return $this->res200($allproduk);
    }

    public function all(Request $req) {
        if (!$req->has('q')) {
            return $this->res200(Produk::orderBy('created_at', 'desc')->get());
        }
        
        return $this->res200(Produk::where('nama', 'like', '%'.$req->q.'%')->get());
    }

    public function one($id) {
        return $this->res200(Produk::find($id));
    }
}
