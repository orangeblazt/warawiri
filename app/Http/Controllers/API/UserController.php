<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use App\User;

class UserController extends Controller
{
    public function getACake() {
        return $this->res200($this->generateCake());
    }

    public function registerUser(Request $req) {
        $existing_user = User::where('email', $req->email)->first();
        if ($existing_user) {
            return $this->res400('Email sudah digunakan.');
        }

        $user = new User();

        $user->nama     = $req->nama;
        $user->email    = $req->email;
        $user->password = bcrypt($req->password);
        $user->alamat   = $req->alamat;
        $user->no_hp    = $req->no_hp;
        $user->token    = ".";

        $user->save();

        return $this->res200($user);
    }

    public function loginUser(Request $req) {
        $existing_user = User::where('email', $req->email)->first();
        if (!$existing_user) {
            return $this->res404('Email tidak ditemukan.');
        }

        if(!Hash::check($req->password, $existing_user->password)) {
            return $this->res400('Password salah.');
        }

        return $this->res200($existing_user);
    }

    private function generateCake() {
        return 'cake-' . strtoupper(str_random(27));
    }
}
