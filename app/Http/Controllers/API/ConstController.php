<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PartnerDonasi;
use App\JasaPengiriman;
use App\Http\Controllers\Admin\MetodePembayaranController;

class ConstController extends Controller
{
    public function daftarPartnerDonasi() {
        $allproduk = PartnerDonasi::orderBy('created_at', 'desc')->get();
        return $this->res200($allproduk);
    }

    public function daftarJasaPengiriman() {
        $allproduk = JasaPengiriman::orderBy('created_at', 'desc')->get();
        return $this->res200($allproduk);
    }

    public function daftarMetodePembayaran() {
        $metode_pembayaran = MetodePembayaranController::$metode_pembayaran;
        return $this->res200($metode_pembayaran);
    }
}
