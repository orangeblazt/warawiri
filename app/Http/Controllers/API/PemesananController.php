<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pemesanan;
use App\Produk;
use App\ProdukPs;

class PemesananController extends Controller
{
    public function create(Request $req) {

        /**
         * Validasi Pemesanan
         */
        if (!$this->validasiPemesanan($req->get('items'))) {
            return $this->res400("Pemesanan Anda tidak dapat dilanjutkan beberapa barang sudah tidak tersedia dalam jumlah yang Anda pesan");
        }

        $pemesanan = new Pemesanan();

        $editable_attrs = [
            'nama_pembeli', 'email', 'nomor_hp', 
            'jasa_pengiriman', 'alamat_pengiriman', 'catatan_pengiriman',
            'target_donasi', 'nominal_donasi'
        ];

        $pemesanan->kode = $this->generateKodePemesanan();
        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $pemesanan[$field] = $req->get($field);
            }
        }

        $pemesanan->owner = $req->cake;
        $pemesanan->owner_id = $req->user_id;
        $pemesanan->save();
        $this->createProdukPemesanan($pemesanan, $req->get('items'));

        $out_pemesanan = Pemesanan::find($pemesanan->id);
        \Mail::to($out_pemesanan->email)->queue(new \App\Mail\Pemesanan($out_pemesanan));

        return $this->res200($out_pemesanan, true);
    }

    public function all(Request $req) {
        if ($req->user_id) {
            return $this->res200(Pemesanan::where('owner', $req->cake)->orWhere('owner_id', $req->user_id)->orderBy('created_at', 'desc')->get());
        } else {
            return $this->res200(Pemesanan::where('owner', $req->cake)->orderBy('created_at', 'desc')->get());
        }
    }

    public function all_count(Request $req) {
        if ($req->user_id) {
            return $this->res200(Pemesanan::where('owner', $req->cake)->orWhere('owner_id', $req->user_id)->count());
        } else {
            return $this->res200(Pemesanan::where('owner', $req->cake)->count());
        }
    }

    public function one($id) {
        $pemesanan = Pemesanan::find($id);
        if ($pemesanan) {
            return $this->res200($pemesanan);
        } else {
            return $this->res404("Pemesanan dengan id=$id tidak ditemukan");
        }
    }

    private function generateKodePemesanan() {
        return 'HM-' . strtoupper(str_random(7));
    }

    private function validasiPemesanan($produk_datas) {
        foreach ($produk_datas as $pdata) {
            $produk = Produk::find($pdata['id']);
            if ($produk->stok < $pdata['qty']) {
                return false;
            }
        }

        return true;
    }

    private function createProdukPemesanan(Pemesanan $pemesanan, $produk_datas) {
        $out = [];
        foreach ($produk_datas as $pdata) {
            $produk = Produk::find($pdata['id']);
            $produk_pemesanan = new ProdukPs();

            $produk->qty = $pdata['qty'];
            $produk->pemesanan_id = $pemesanan->id;
            $editable_attrs = [
                'pemesanan_id', 'nama', 'img_url', 
                'video_url', 'qty', 'harga'
            ];

            foreach ($editable_attrs as $field) {
                $produk_pemesanan[$field] = $produk[$field];
            }

            $produk_pemesanan->save();
            $produk->stok       -= $produk->qty;
            $produk->terjual    += $produk->qty;
            
            unset($produk->qty);
            unset($produk->pemesanan_id);
            $produk->save();

            array_push($out, $produk_pemesanan);
        }

        return $out;
    }
}
