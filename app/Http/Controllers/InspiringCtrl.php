<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Foundation\Inspiring;

class InspiringCtrl extends Controller {
    public function quote() {
        return $this->res200(Inspiring::quote());
    }
}
