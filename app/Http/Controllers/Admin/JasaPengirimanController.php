<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\JasaPengiriman;

class JasaPengirimanController extends Controller
{
    public function all() {
        $allproduk = JasaPengiriman::orderBy('created_at', 'desc')->get();
        header('Content-Range: ' . sizeof($allproduk));
        return $this->resOk($allproduk, true);
    }

    public function one($id) {
        return $this->resOk(JasaPengiriman::find($id), true);
    }

    public function put($id, Request $req) {
        $produk = JasaPengiriman::find($id);
        
        $editable_attrs = [
            'nama', 'deskripsi'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }

    public function delete($id) {
        $produk = JasaPengiriman::find($id);
        return $this->resOk($produk->delete(), true);
    }

    public function create(Request $req) {
        $produk = new JasaPengiriman();

        $editable_attrs = [
            'nama', 'deskripsi'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }
}
