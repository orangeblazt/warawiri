<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StatusPemesananController extends Controller
{
    public static $status_pemesanan = [
        [
            "id" => 99,
            "nama" => "Dibatalkan"
        ], 
        [
            "id" => 1,
            "nama" => "Pembayaran Belum Lunas"
        ], 
        [
            "id" => 2,
            "nama" => "Pembayaran Terkonfirmasi"
        ], 
        [
            "id" => 3,
            "nama" => "Pemesanan Sedang Diproses"
        ], 
        [
            "id" => 4,
            "nama" => "Dalam Pengiriman"
        ], 
        [
            "id" => 5,
            "nama" => "Pengiriman Telah Sampai"
        ]
    ];

    public function all() {
        $status_pemesanan = StatusPemesananController::$status_pemesanan;
        header('Content-Range: ' . sizeof($status_pemesanan));
        return $this->resOk($status_pemesanan, true);
    }
}
