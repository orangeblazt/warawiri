<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\Produk;

class ProdukController extends Controller
{

    public function all() {
        $allproduk = Produk::orderBy('created_at', 'desc')->get();
        header('Content-Range: ' . sizeof($allproduk));
        return $this->resOk($allproduk, true);
    }

    public function one($id) {
        return $this->resOk(Produk::find($id), true);
    }

    public function put($id, Request $req) {
        $produk = Produk::find($id);

        if ($req->has('img_url')) {
            $produk['img_url'] = $this->newImage($req->img_url);
        }
        
        $editable_attrs = [
            'nama', 'stok', 'harga', 'deskripsi', 'video_url'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }

    public function delete($id) {
        $produk = Produk::find($id);
        return $this->resOk($produk->delete(), true);
    }

    public function create(Request $req) {
        $produk = new Produk();

        if ($req->has('img_url')) {
            $produk['img_url'] = $this->newImage($req->img_url);
        }
        
        $editable_attrs = [
            'nama', 'stok', 'harga', 'deskripsi', 'video_url'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }

    public function newImage($image64) {
        if (is_string($image64) && substr($image64, 0, 4) == 'http') {
            return $image64;
        }

        $extensions = pathinfo($image64['title'], PATHINFO_EXTENSION);
        
        $image = $image64['src'];
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace('data:image/jpg;base64,', '', $image);
        $image = str_replace('data:image/jpeg;base64,', '', $image);
        $image = str_replace(' ', '+', $image);

        $imageName = str_random(10) . '.' . $extensions;
        Storage::disk('public')->put($imageName, base64_decode($image));
        return URL::to('/storage/app/public/' . $imageName);
    }
}
