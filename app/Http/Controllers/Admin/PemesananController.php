<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Pemesanan;

class PemesananController extends Controller
{
    public function all() {
        $allpemesanan = Pemesanan::orderBy('created_at', 'desc')->get();
        header('Content-Range: ' . sizeof($allpemesanan));
        return $this->resOk($allpemesanan, true);
    }

    public function one($id) {
        return $this->resOk(Pemesanan::find($id), true);
    }
    
    public function put($id, Request $req) {
        $pemesanan = Pemesanan::find($id);

        $editable_attrs = [
            'status_pemesanan'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $pemesanan[$field] = $req->get($field);
            }
        }

        $pemesanan->save();

        return $this->resOk($pemesanan, true);   
    }

    public function delete($id) {
        $pemesanan = Pemesanan::find($id);
        return $this->resOk($pemesanan->delete(), true);
    }
}
