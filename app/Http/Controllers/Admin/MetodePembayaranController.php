<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MetodePembayaranController extends Controller
{
    public static $metode_pembayaran = [
        [
            "id" => 1,
            "nama" => "Transfer",
            "class_name" => "transfer",
            "deskripsi" => "Lakukan pembayaran ke nomor rekening 1234567 a.n. John Smith",
            "image" => "https://image.cermati.com/c_fit,fl_progressive,h_240,q_80,w_360/v1/sharia/rkm9ujeukngirlufgefq.png"
        ], 
        [
            "id" => 2,
            "nama" => "Indomaret",
            "class_name" => "indomaret",
            "deskripsi" => "Bayar melalui indomaret terdekat",
            "image" => "https://s.kaskus.id/images/2018/11/19/10413092_20181119010947.png"
        ], 
        [
            "id" => 3,
            "nama" => "VISA/Mastercard",
            "class_name" => "cc",
            "deskripsi" => "Bayar dengan kartu kredit",
            "image" => "https://securecdn.pymnts.com/wp-content/uploads/2019/04/Visa-Digital-First-Platform.png"
        ]
    ];

    public function all() {
        $metode_pembayaran = MetodePembayaranController::$metode_pembayaran;
        header('Content-Range: ' . sizeof($metode_pembayaran));
        return $this->resOk($metode_pembayaran, true);
    }
}
