<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PartnerDonasi;

class PartnerDonasiController extends Controller
{
    public function all() {
        $allproduk = PartnerDonasi::orderBy('created_at', 'desc')->get();
        header('Content-Range: ' . sizeof($allproduk));
        return $this->resOk($allproduk, true);
    }

    public function one($id) {
        return $this->resOk(PartnerDonasi::find($id), true);
    }

    public function put($id, Request $req) {
        $produk = PartnerDonasi::find($id);
        
        $editable_attrs = [
            'nama', 'alamat'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }

    public function delete($id) {
        $produk = PartnerDonasi::find($id);
        return $this->resOk($produk->delete(), true);
    }

    public function create(Request $req) {
        $produk = new PartnerDonasi();

        $editable_attrs = [
            'nama', 'alamat'
        ];

        foreach ($editable_attrs as $field) {
            if ($req->has($field)) {
                $produk[$field] = $req->get($field);
            }
        }

        $produk->save();

        return $this->resOk($produk, true);
    }
}
