<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    
    protected function resOk($data, $is_admin = false) {
        if($is_admin){
            return response()->json($data);
        }
        return response()->json([
            'result'    => $data,
            'error'     => null
        ]);
    }
    
    protected function resNotOk($error, $is_admin = false) {
        if($is_admin){
            return response()->json($data);
        }
        return response()->json([
            'result'    => null,
            'error'     => $error
        ]);
    }

    protected function res200($data) {
        return $this->resOk($data);
    }
    
    protected function res201($data) {
        return $this->resOk($data);
    }
    
    protected function res400($error) {
        return $this->resNotOk($error);
    }
    
    protected function res404($error) {
        return $this->resNotOk($error);
    }
    
    protected function res500($error) {
        return $this->resNotOk($error);
    }
}
