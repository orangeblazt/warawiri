<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Controllers\Admin\StatusPemesananController;

class Pemesanan extends Model
{
    protected $with = ['items', 'jasaPengiriman', 'targetDonasi'];
    protected $appends = ['status'];

    public function getStatusAttribute() {
        $newsp = [
            "id" => $this->status_pemesanan,
            "nama" => "-"
        ];

        $all_sp = StatusPemesananController::$status_pemesanan;
        foreach ($all_sp as $sp) {
            if ($sp["id"] == $newsp["id"]) {
                $newsp["nama"] = $sp["nama"];
            }
        }

        return $newsp;
    }

    public function jasaPengiriman() {
        return $this->hasOne('App\JasaPengiriman', 'id', 'jasa_pengiriman');
    }

    public function targetDonasi() {
        return $this->hasOne('App\PartnerDonasi', 'id', 'target_donasi');
    }
    
    public function items() {
        return $this->hasMany('App\ProdukPs', 'pemesanan_id', 'id');
    }
}
