<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

header('Access-Control-Allow-Origin:  *');
header('Access-Control-Allow-Methods:  POST, GET, OPTIONS, PUT, DELETE');
header('Access-Control-Allow-Headers:  Content-Type, X-Auth-Token, Origin, Authorization');
header('Access-Control-Expose-Headers:  Content-Range');
header('Content-Range: 1');
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::get('/quote', 'InspiringCtrl@quote');


/**
 * Application API
 */
Route::prefix('/v1')->group(function () {
    /**
     * Pengguna
     */
    Route::prefix('/user')->group(function() {
        Route::post('/login',       'API\\UserController@loginUser');
        Route::post('/register',    'API\\UserController@registerUser');
    });

    /**
     * Produk
     */
    Route::prefix('/produk')->group(function () {
        Route::get('/kategori/terlaris',    'API\\ProdukController@terlaris');
        Route::get('/kategori/terbaru',     'API\\ProdukController@terbaru');
        Route::get('/',                     'API\\ProdukController@all');
        Route::get('/{id}',                 'API\\ProdukController@one');
    });

    /**
     * Pemesanan
     */
    Route::prefix('/pemesanan')->group(function () {
        Route::get('/',         'API\\PemesananController@all');
        Route::get('/count/all',         'API\\PemesananController@all_count');
        Route::get('/{id}',         'API\\PemesananController@one');
        Route::post('/',        'API\\PemesananController@create');
    });

    /**
     * Konstanta
     */
    Route::prefix('/const')->group(function () {
        Route::get('/partner_donasi',   'API\\ConstController@daftarPartnerDonasi');
        Route::get('/jasa_pengiriman',  'API\\ConstController@daftarJasaPengiriman');
        Route::get('/metode_pembayaran',  'API\\ConstController@daftarMetodePembayaran');
    });

    /**
     * Etc
     */
    Route::get('/give_me_a_cake',  'API\\UserController@getACake');
    Route::get('/test',  function (Request $req) {
        dd($req->cake);
    });
});

/**
 * Admin API
 */
Route::prefix('/admin/v1')->group(function () {
    /**
     * Produk
     */
    Route::prefix('/produk')->group(function () {
        Route::get('/',         'Admin\\ProdukController@all');
        Route::get('/{id}',     'Admin\\ProdukController@one');
        Route::put('/{id}',     'Admin\\ProdukController@put');
        Route::delete('/{id}',  'Admin\\ProdukController@delete');
        Route::post('/',        'Admin\\ProdukController@create');
    });

    /**
     * Pemesanan
     */
    Route::prefix('/pemesanan')->group(function () {
        Route::get('/',         'Admin\\PemesananController@all');
        Route::get('/{id}',     'Admin\\PemesananController@one');
        Route::put('/{id}',     'Admin\\PemesananController@put');
        Route::delete('/{id}',  'Admin\\PemesananController@delete');
        Route::post('/',        'Admin\\PemesananController@create');
    });

    /**
     * Jasa Pengiriman
     */
    Route::prefix('/jasa_pengiriman')->group(function () {
        Route::get('/',         'Admin\\JasaPengirimanController@all');
        Route::get('/{id}',     'Admin\\JasaPengirimanController@one');
        Route::put('/{id}',     'Admin\\JasaPengirimanController@put');
        Route::delete('/{id}',  'Admin\\JasaPengirimanController@delete');
        Route::post('/',        'Admin\\JasaPengirimanController@create');
    });

    /**
     * Partner Donasi
     */
    Route::prefix('/partner_donasi')->group(function () {
        Route::get('/',         'Admin\\PartnerDonasiController@all');
        Route::get('/{id}',     'Admin\\PartnerDonasiController@one');
        Route::put('/{id}',     'Admin\\PartnerDonasiController@put');
        Route::delete('/{id}',  'Admin\\PartnerDonasiController@delete');
        Route::post('/',        'Admin\\PartnerDonasiController@create');
    });

    /**
     * Etc
     */
    Route::get('/status_pemesanan',  'Admin\\StatusPemesananController@all');
});
