<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});
Route::get('/autentikasi', function () {
    return view('autentikasi');
});
Route::get('/profil', function () {
    return view('profil');
});
Route::get('/keranjang', function () {
    return view('keranjang');
});
Route::get('/checkout', function () {
    return view('checkout');
});
Route::get('/pencarian', function () {
    return view('pencarian');
});
Route::get('/pemesanan', function () {
    return view('pemesanan');
});
Route::get('/produk/{id}', function () {
    return view('detail-produk');
});
Route::get('/pemesanan/{id}', function () {
    return view('detail-pemesanan');
});
Route::get('/pemesanan/{id}/metode-pembayaran', function ($id) {
    return view('metode-pembayaran', [
        'pemesanan_id' => $id
    ]);
});


Route::get('/chat', function () {
    return view('daftar-chat');
});
Route::get('/chat/{id}', function () {
    return view('chat');
});